package com.geektechnique.jackson.controller;

import com.geektechnique.jackson.dtos.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @RequestMapping("/")
    public User user(){
        return User.builder()
                .name("Jack")
                .age(27)
                .build();
    }
}
